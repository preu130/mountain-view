# Mountain-View

A little app to search mountains on Wikidata and map them. In the long term, the app is supposed to provide a dashboard to analyse and visualise your mountain ascents. 

You can find the app at [https://friep.shinyapps.io/mountain_view/](https://friep.shinyapps.io/mountain_view/). Contact me if you cannot access the app. 